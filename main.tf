terraform {
  backend "http" {}
}

module "gitlab_terraform_module_registry_example" {
  source  = "codebase.helmholtz.cloud/hueser93/gitlab-terraform-module-registry-example/local"
  version = "0.0.1"

  username = "Christian"
}

